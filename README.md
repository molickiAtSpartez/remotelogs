### Basic usage
`go build && ./logsAggregator`

#### Options:
* -port[9999] - Port to listen on, 9999 by default
* -file["logs.log"] - File to store logs. `logs.log` by default
* -silent - if provided, will only put logs into file, but not into standard output

*** 

### Available also as Docker Image:
https://hub.docker.com/r/grzegorzmolicki/remote-logs-aggregator

#### Quick run
`docker run -p 9999:9999 grzegorzmolicki/remote-logs-aggregator`