FROM alpine:3.7
COPY ./alpineLogsAggregator ./logsAggregator
EXPOSE 9999
CMD ["./logsAggregator"]
