package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"
)

func checkError(err error) {
	if err != nil {
		panic(err.Error())
	}
}

func checkPortInRange(port *int) {
	if *port < 1 || *port > 65535 {
		panic("Port out of range")
	}
}

func createLogFileIfNotExists(filePath *string) {
	_, fileOpenError := os.Open(*filePath)
	if fileOpenError != nil {
		file, fileCreateError := os.Create(*filePath)
		if fileCreateError != nil {
			panic(fileCreateError)
		} else {
			file.Close()
		}
	}
}

func aggregate(request *http.Request, silent *bool, logsFile *string) {
	if request.Method == "POST" {
		bytes, _ := ioutil.ReadAll(request.Body)
		createLogFileIfNotExists(logsFile)
		file, err := os.OpenFile(*logsFile, os.O_APPEND|os.O_RDWR, 0666)
		checkError(err)
		msg := fmt.Sprintf("[%s] %s\n", time.Now().Format("02-01-2006 15:04:05"), string(bytes))
		if *silent == false {
			fmt.Print(msg)
		}
		_, err = file.WriteString(msg)
		checkError(err)
		err = file.Close()
		checkError(err)
	}
}

func startServer(port *int, silent *bool, logsFile *string) {
	http.HandleFunc("/", func(writer http.ResponseWriter, request *http.Request) {
		aggregate(request, silent, logsFile)
	})
	err := http.ListenAndServe(fmt.Sprintf(":%d", *port), nil)
	if err != nil {
		fmt.Println("Could not start server, err: ", err)
	}
}

var Usage = func() {
	fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])

	flag.PrintDefaults()
}

func main() {
	port := flag.Int("port", 9999, "Port to listen on")
	silent := flag.Bool("silent", false, "If provided, will not print received messages to standard output")
	logsFile := flag.String("file", "./logs.log", "File to store logs in. 'logs.log' by default. If specified does not exist, it will be created")
	flag.Parse()
	checkPortInRange(port)
	fmt.Printf("Server starting on port %d...\n", *port)
	startServer(port, silent, logsFile)
}
